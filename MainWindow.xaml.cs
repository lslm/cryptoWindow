﻿using System.Windows;

namespace CryptoWindow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Validate validation;

        public MainWindow()
        {
            InitializeComponent();
            validation = new Validate();

        }

        private void txtPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            validation.Password = txtPassword.Text;
        }

        private void txtConfirmPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            validation.ConfirmPassword = txtConfirmPassword.Text;
        }

        private void btnEncrypt_Click(object sender, RoutedEventArgs e)
        {
            lblHash.Content = validation.Authenticate();
        }
    }
}
